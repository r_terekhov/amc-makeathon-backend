/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * vk.com/r_terekhov
 * Copyright (c) 2020.
 */

package terekhov.amc.makeathon.backend.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@Configuration
@EnableMongoRepositories(basePackages = {"terekhov.amc.makeathon.backend.repository"})
@EnableAutoConfiguration
@Slf4j
public class MongoConfiguration {
}

//    @EventListener(ApplicationReadyEvent.class)
//    public void initIndicesAfterStartup() {
//
//        log.info("Mongo InitIndicesAfterStartup init");
//
//        MappingContext<? extends MongoPersistentEntity<?>, MongoPersistentProperty> mappingContext = this.mongoConverter.getMappingContext();
//
//        if (mappingContext instanceof MongoMappingContext) {
//            MongoMappingContext mongoMappingContext = (MongoMappingContext) mappingContext;
//            for (BasicMongoPersistentEntity<?> persistentEntity : mongoMappingContext.getPersistentEntities()) {
//                Class<?> clazz = persistentEntity.getType();
//                if (clazz.isAnnotationPresent(Document.class)) {
//                    MongoPersistentEntityIndexResolver resolver = new MongoPersistentEntityIndexResolver(mongoMappingContext);
//
//                    IndexOperations indexOps = mongoTemplate.indexOps(clazz);
//                    resolver.resolveIndexFor(clazz).forEach(indexOps::ensureIndex);
//                }
//            }
//        }
//    }
