package terekhov.amc.makeathon.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import terekhov.amc.makeathon.backend.dto.PlaceDto;
import terekhov.amc.makeathon.backend.model.BaseResponse;
import terekhov.amc.makeathon.backend.service.PlaceService;

import java.util.List;

@RestController
public class PlaceController {
    private static final Logger log = LoggerFactory.getLogger(PlaceController.class);
    private final PlaceService placeService;

    public PlaceController(PlaceService placeService) {
        this.placeService = placeService;
    }


    @GetMapping("/place/getAll")
    public ResponseEntity<BaseResponse<List<PlaceDto>>> getAllPlaces() {
        return new ResponseEntity<>(BaseResponse.response(placeService.getAllPlaces()), HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
