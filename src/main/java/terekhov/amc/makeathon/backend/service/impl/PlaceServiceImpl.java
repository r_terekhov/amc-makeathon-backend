package terekhov.amc.makeathon.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import terekhov.amc.makeathon.backend.dto.PlaceDto;
import terekhov.amc.makeathon.backend.repository.PlaceRepository;
import terekhov.amc.makeathon.backend.service.PlaceService;
import terekhov.amc.makeathon.backend.utils.Utils;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlaceServiceImpl implements PlaceService {
    private static final Logger log = LoggerFactory.getLogger(PlaceService.class);
    private final PlaceRepository placeRepository;

    public PlaceServiceImpl(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }

    @Override
    public List<PlaceDto> getAllPlaces() {
        return placeRepository.findAll().stream().map(place -> {
            List<Double> latLng = Utils.convertLocationToLatLng(place.getLocation());
            return new PlaceDto(place.getName(), place.getCategory(), place.getUrlPhoto(), place.getDescription(),
                    latLng.get(0), latLng.get(1), place.getCurrentFullness(), place.getMaxFullness(), place.getWorkingHours(), place.getQueueId());
        }).collect(Collectors.toList());
    }
}
