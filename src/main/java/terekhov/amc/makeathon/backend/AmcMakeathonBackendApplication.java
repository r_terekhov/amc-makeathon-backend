package terekhov.amc.makeathon.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmcMakeathonBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmcMakeathonBackendApplication.class, args);
    }

}
