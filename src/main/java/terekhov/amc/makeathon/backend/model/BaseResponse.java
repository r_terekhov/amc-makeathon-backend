package terekhov.amc.makeathon.backend.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
public class BaseResponse<T> {
    private T response;
    private String error;

    private BaseResponse(T response, String error) {
        this.response = response;
        this.error = error;
    }

    public static <T> BaseResponse<T> response(T response) {
        return new BaseResponse<>(response, null);
    }

    public static <T> BaseResponse<T> error(String error) {
        return new BaseResponse<>(null, error);
    }
}
