package terekhov.amc.makeathon.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import terekhov.amc.makeathon.backend.dto.QueueDto;
import terekhov.amc.makeathon.backend.model.BaseResponse;
import terekhov.amc.makeathon.backend.service.QueueService;

@RestController
public class QueueController {
    private static final Logger log = LoggerFactory.getLogger(QueueController.class);
    private final QueueService queueService;

    public QueueController(QueueService queueService) {
        this.queueService = queueService;
    }


    @PostMapping("/queue/join")
    public ResponseEntity<BaseResponse<QueueDto>> join(
            @RequestParam String queueId) {
        return new ResponseEntity<>(BaseResponse.response(queueService.joinQueue(queueId)), HttpStatus.OK);
    }

    @GetMapping("/queue/getStatus")
    public ResponseEntity<BaseResponse<QueueDto>> getStatus(
            @RequestParam String innerQueueId) {
        return new ResponseEntity<>(BaseResponse.response(queueService.getStatus(innerQueueId)), HttpStatus.OK);
    }

    @PostMapping("/queue/leave")
    public ResponseEntity<BaseResponse<String>> leave(
            @RequestParam String innerQueueId) {
        return new ResponseEntity<>(BaseResponse.response(queueService.leaveQueue(innerQueueId)), HttpStatus.OK);
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
