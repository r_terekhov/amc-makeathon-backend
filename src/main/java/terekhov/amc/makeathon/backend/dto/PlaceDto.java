package terekhov.amc.makeathon.backend.dto;

import lombok.Value;


@Value
public class PlaceDto {

    String name;

    String category;

    String urlPhoto;

    String description;

    double lat;

    double lng;

    int currentFullness;

    int maxFullness;

    String workingHours;

    String queueId;
}
