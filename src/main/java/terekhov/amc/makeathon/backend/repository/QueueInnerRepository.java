/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package terekhov.amc.makeathon.backend.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import terekhov.amc.makeathon.backend.domain.QueueInner;

import java.util.List;

@Repository
public interface QueueInnerRepository extends MongoRepository<QueueInner, String> {

    List<QueueInner> findAllByCreatedBefore(long now);
}
