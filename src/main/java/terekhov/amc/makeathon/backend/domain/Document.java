package terekhov.amc.makeathon.backend.domain;

import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.Version;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@org.springframework.data.mongodb.core.mapping.Document
public abstract class Document {
    @Transient
    private static final Double CURRENT_SCHEMA_VERSION = 1.0;

    @Id
    @Getter
    @NotBlank
    private String id;


    @Getter
    @NotNull
    private long created;

    @Version
    @Getter
    @NotNull
    private int version;

    @Getter
    @NotNull
    private Double schemaVersion = CURRENT_SCHEMA_VERSION;

    public Document() {
        this.created = Instant.now().getEpochSecond();
    }


}
