package terekhov.amc.makeathon.backend.service.impl;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import terekhov.amc.makeathon.backend.domain.Place;
import terekhov.amc.makeathon.backend.domain.Queue;
import terekhov.amc.makeathon.backend.domain.QueueInner;
import terekhov.amc.makeathon.backend.repository.PlaceRepository;
import terekhov.amc.makeathon.backend.repository.QueueInnerRepository;
import terekhov.amc.makeathon.backend.repository.QueueRepository;
import terekhov.amc.makeathon.backend.utils.Utils;


@DataMongoTest
@ActiveProfiles("local")
@EnableMongoRepositories(basePackages = {"terekhov.amc.makeathon.backend.repository"})
@RunWith(SpringRunner.class)
@Ignore
public class PlaceServiceImplTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private PlaceRepository placeRepository;

    @Autowired
    private QueueRepository queueRepository;

    @Autowired
    private QueueInnerRepository queueInnerRepository;


    @org.junit.Before
    public void setUp() {
       /* mongoTemplate.indexOps("hubCategory").dropAllIndexes();
        mongoTemplate.indexOps("hub").dropAllIndexes();*/
        placeRepository.deleteAll();
        queueRepository.deleteAll();
        queueInnerRepository.deleteAll();
    }


    @Test
    public void savePlace() {
        Queue queue = queueRepository.save(new Queue("Пушкин", 20));

        Place place = placeRepository.save(new Place(
                "Пушкин", "Еда", "https://img04.rl0.ru/afisha/o/s4.afisha.ru/mediastorage/88/22/32674b229ea147dabf33efce2288.jpg",
                "Аристократичный ресторан русской кухни\n" +
                        "До сих пор ресторан номер один в Москве — место, куда сразу же ведут любого приезжего иностранца. Несмотря на интерьер, универсальное заведение, работающее круглосуточно и подходящее для любого случая.",
                new GeoJsonPoint(new Point(37.62807084, 55.75420654)),
                5, 50, "09:00 - 23:00", queue.getId()));


        queueInnerRepository.save(new QueueInner(queue.getId(), Utils.randomString()));
    }
}