FROM adoptopenjdk/openjdk11:latest

ADD . /app
WORKDIR /app

ENV TZ=Etc/UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


WORKDIR /app
COPY target/*.jar amc-makeathon-backend-0.0.1.jar
ENV LOG_LEVEL_APP=INFO
ENV LOG_LEVEL_ROOT=INFO
ADD ./docker-entry.sh .
RUN chmod +x ./docker-entry.sh
CMD ./docker-entry.sh
EXPOSE 8080