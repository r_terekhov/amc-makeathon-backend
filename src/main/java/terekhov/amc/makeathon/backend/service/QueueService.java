package terekhov.amc.makeathon.backend.service;

import terekhov.amc.makeathon.backend.dto.QueueDto;

public interface QueueService {


    QueueDto joinQueue(String queueId);

    QueueDto getStatus(String innerQueueId);

    String leaveQueue(String innerQueueId);
}
