package terekhov.amc.makeathon.backend.service;

import terekhov.amc.makeathon.backend.dto.PlaceDto;

import java.util.List;

public interface PlaceService {

    List<PlaceDto> getAllPlaces();

}
