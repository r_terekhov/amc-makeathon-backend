package terekhov.amc.makeathon.backend.dto;

import lombok.Value;

@Value
public class QueueDto {

    String queueInnerId;

    String code;

    String place;

    int waitingTime;

    int beforeYou;

}
